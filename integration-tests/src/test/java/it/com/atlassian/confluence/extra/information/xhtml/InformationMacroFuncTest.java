package it.com.atlassian.confluence.extra.information.xhtml;

public class InformationMacroFuncTest extends AbstractConfluencePluginWebTestCaseBase {
    private static final String MACRO_NAME = "info";
    private static final String TEST_SPACE_KEY = "tst";

    protected void setUp() throws Exception {
        super.setUp();
        createSpace(TEST_SPACE_KEY, "Test", "Test space");
    }

    @Override
    protected void tearDown() throws Exception {
        deleteSpace(TEST_SPACE_KEY);
        super.tearDown();
    }

    public void testInfoMacro() {
        long testPageId = createPage(TEST_SPACE_KEY, "testInfoMacro", "{" + MACRO_NAME + ":title=Monkey Trousers}All the monkeys{" + MACRO_NAME + "}");

        viewPageById(testPageId);

        assertTextPresent("Monkey Trousers");
        assertTextPresent("All the monkeys");
    }

    public void testInfoMacroWithAnotherMacroInside() {
        long testPageId = createPage(TEST_SPACE_KEY, "testInfoMacro", "{" + MACRO_NAME + ":title=Monkey Trousers}{cheese}{" + MACRO_NAME + "}");

        viewPageById(testPageId);

        assertTextPresent("Monkey Trousers");
        assertTextPresent("I like cheese!");
    }

    public void testInfoMacroWithNewLineAndNoTitle() {
        final long testPageId = createPage(TEST_SPACE_KEY, "testInfoMacroWithNewLineAndNoTitle", "{" + MACRO_NAME + "}\n" +
                "\\\\\n" +
                "{" + MACRO_NAME + "}");

        viewPageById(testPageId);

        assertElementNotPresentByXPath("//div[contains(concat(' ',normalize-space(@class),' '),' panelMacro ') and contains(concat(' ',normalize-space(@class),' '),' infoMacro ')]//h6[@class='title']");
    }

    public void testInfoMacroInRTE() {
        String macroBody = "some crazy text";
        final long testPageId = createPage(TEST_SPACE_KEY, "testInfoMacroWithNoNewLine", "{" + MACRO_NAME + "}" +
                macroBody + "{" + MACRO_NAME + "}");

        editPageById(testPageId);

        // Wysiwyg text is escaped inside the textarea
        String wysiwygText = getElementById("wysiwygTextarea").getTextContent();

        assertFalse(wysiwygText.contains("<pre>" + macroBody + "</pre>"));
        assertTrue(wysiwygText.contains("<p>" + macroBody + "</p>"));
    }
}